<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasTax;

class ExpenseItem extends Model
{

    use HasTax;

    protected $table = "expense_item";

    protected $fillable = [
        'expense_category_id',
        'expense_id',
        'company_id',
        'description',
        'price',
        'total'
    ];

    public function scopeFindByCompany($query, $company_id)
    {
        $query->where('company_id', $company_id);
    }

    public function expense()
    {
        return $this->belongsTo(Expense::class);
    }

    public function category()
    {
        return $this->belongsTo(ExpenseCategory::class, 'expense_category_id');
    }

      /**
     * Get the Total Percentage of Estimate Item Taxes
     * 
     * @return int
     */
    public function getTotalPercentageOfTaxes()
    {
        $total = 0;
        foreach ($this->taxes as $tax) {
            $total += $tax->tax_type->percent;
        }

        return (int) $total;
    }

    /**
     * Get the Total Percentage of Invoice Item Taxes with Tax Names
     * 
     * @return array 
     */
    public function getTotalPercentageOfTaxesWithNames()
    {
        $total = [];
        foreach ($this->taxes as $tax) {
            if (isset($total[$tax->tax_type->name])) {
                $total[$tax->tax_type->name] += $tax->tax_type->percent;
            } else {
                $total[$tax->tax_type->name] = $tax->tax_type->percent;
            }
        }

        return $total;
    }
}
