<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Laravel\Socialite\Facades\Socialite;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Exception;
use App\Models\Company;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';
//     protected function redirectTo()
//     {
        
//         if (auth()->user()->hasRole('super_admin'))
//         {
//             return '/admin/dashboard';
//         }
//         if (auth()->user()->hasRole('admin'))
//         {
//             return ''.auth()->user()->uid.'/dashboard';
//         }
//         if (auth()->user()->hasRole('staff'))
//         {
          
//             return '/'.auth()->user()->uid.'/dashboard';
//         }   
//         // return '/';

//    }
    

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function showLoginForm()
    // {
    //     return view('auth.login');

    // }
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToGoogle()
    {

        
        return Socialite::driver('google')->redirect();
    }
   
    public function handleGoogleCallback()
    {

        
        try {
            // dd('hii'); 
  
            $user = Socialite::driver('google')->user();



          
            // dd($user->id);
        
   
            $finduser = User::where('email', $user->email)->first();

           
        // dd($finduser);
   
            if($finduser){
   
                if($finduser->hasRole('admin')){
                    
                    Auth::login($finduser);
                    // dd($finduser->uid);
      
                    return redirect()->route('super_admin.dashboard');
                }
   
            }else{
                // $new = new User();
                $newUser = User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    // 'first_name' => $user->given_name,
                    // 'last_name' => $user->family_name,
                    'provider_id'=> $user->id,
                    'state_id' => 33
                   
                ]);

                $id = User::where('provider_id', $user->id)->first();

                // 

                 // Create Company
                    $company = Company::create([
                        'name' => 'My Awesome Company',
                        'owner_id' => $id->id,
                    ]);

                // dd($newUser);

                  // Assign Role
                $newUser->assignRole("admin");

                // Attach User to Company
                $newUser->attachCompany($company);
  
                Auth::login($newUser);
                return redirect()->route('dashboard', ['company_uid' => $company->uid]);
            }
  
        } catch (Exception $e) {

            // dd($e);  
            return redirect('auth/google');
        }

        // dd($user);

    }


}
