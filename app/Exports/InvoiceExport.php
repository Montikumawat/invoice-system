<?php

use App\Models\Invoice;
use Maatwebsite\Excel\Concerns\FromCollection;
  
class UsersExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Invoice::all();
    }

    public function headings(): array
    {
        return [
            'email',
            'invoice_number',
            'reference_number',
            'status',
            'attachment',
            'paid_status',
            'tax_per_item',
            'discount_per_item',
            'notes',
            'private_notes',
            'discount_type',
            'discount_val',
            'sub_total',
            'total',
            'due_amount',
            'product_description',
            'product_price',
            'product_quantity',
            'product_discount_type',
            'product_discount_val',
            'product_total'
        ];
    }

    /**
    * @var Product $product
    */
    // public function map($product): array
    // {
    //     return [
    //         $product->name,
    //         $product->added_by,
    //         $product->user_id,
    //         $product->category_id,
    //         $product->brand_id,
    //         $product->year,
    //         $product->model_id,
    //         $product->video_provider,
    //         $product->video_link,
    //         $product->unit_price,
    //         $product->purchase_price,
    //         $product->unit,
    //         $product->current_stock,
    //     ];
    // }
}
