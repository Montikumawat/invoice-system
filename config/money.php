<?php

return [

    'INR' => [
        'name'                => 'Indian Rupee',
        'code'                => 356,
        'precision'           => 2,
        'subunit'             => 100,
        'symbol'              => '₹',
        'symbol_first'        => true,
        'decimal_mark'        => '.',
        'thousands_separator' => ',',
    ],
];
