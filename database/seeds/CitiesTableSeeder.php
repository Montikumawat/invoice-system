<?php

use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        INSERT INTO cities(id, name, state_id) VALUES
        (1, 'Kolhapur', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (2, 'Port Blair', 'Andaman & Nicobar Islands');
        INSERT INTO cities(id, name, state_id) VALUES
        (3, 'Adilabad', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (4, 'Adoni', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (5, 'Amadalavalasa', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (6, 'Amalapuram', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (7, 'Anakapalle', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (8, 'Anantapur', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (9, 'Badepalle', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (10, 'Banganapalle', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (11, 'Bapatla', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (12, 'Bellampalle', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (13, 'Bethamcherla', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (14, 'Bhadrachalam', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (15, 'Bhainsa', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (16, 'Bheemunipatnam', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (17, 'Bhimavaram', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (18, 'Bhongir', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (19, 'Bobbili', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (20, 'Bodhan', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (21, 'Chilakaluripet', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (22, 'Chirala', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (23, 'Chittoor', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (24, 'Cuddapah', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (25, 'Devarakonda', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (26, 'Dharmavaram', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (27, 'Eluru', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (28, 'Farooqnagar', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (29, 'Gadwal', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (30, 'Gooty', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (31, 'Gudivada', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (32, 'Gudur', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (33, 'Guntakal', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (34, 'Guntur', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (35, 'Hanuman Junction', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (36, 'Hindupur', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (37, 'Hyderabad', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (38, 'Ichchapuram', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (39, 'Jaggaiahpet', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (40, 'Jagtial', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (41, 'Jammalamadugu', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (42, 'Jangaon', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (43, 'Kadapa', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (44, 'Kadiri', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (45, 'Kagaznagar', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (46, 'Kakinada', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (47, 'Kalyandurg', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (48, 'Kamareddy', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (49, 'Kandukur', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (50, 'Karimnagar', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (51, 'Kavali', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (52, 'Khammam', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (53, 'Koratla', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (54, 'Kothagudem', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (55, 'Kothapeta', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (56, 'Kovvur', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (57, 'Kurnool', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (58, 'Kyathampalle', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (59, 'Macherla', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (60, 'Machilipatnam', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (61, 'Madanapalle', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (62, 'Mahbubnagar', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (63, 'Mancherial', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (64, 'Mandamarri', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (65, 'Mandapeta', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (66, 'Manuguru', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (67, 'Markapur', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (68, 'Medak', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (69, 'Miryalaguda', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (70, 'Mogalthur', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (71, 'Nagari', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (72, 'Nagarkurnool', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (73, 'Nandyal', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (74, 'Narasapur', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (75, 'Narasaraopet', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (76, 'Narayanpet', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (77, 'Narsipatnam', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (78, 'Nellore', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (79, 'Nidadavole', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (80, 'Nirmal', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (81, 'Nizamabad', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (82, 'Nuzvid', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (83, 'Ongole', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (84, 'Palacole', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (85, 'Palasa Kasibugga', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (86, 'Palwancha', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (87, 'Parvathipuram', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (88, 'Pedana', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (89, 'Peddapuram', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (90, 'Pithapuram', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (91, 'Pondur', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (92, 'Ponnur', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (93, 'Proddatur', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (94, 'Punganur', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (95, 'Puttur', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (96, 'Rajahmundry', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (97, 'Rajam', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (98, 'Ramachandrapuram', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (99, 'Ramagundam', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (100, 'Rayachoti', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (101, 'Rayadurg', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (102, 'Renigunta', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (103, 'Repalle', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (104, 'Sadasivpet', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (105, 'Salur', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (106, 'Samalkot', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (107, 'Sangareddy', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (108, 'Sattenapalle', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (109, 'Siddipet', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (110, 'Singapur', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (111, 'Sircilla', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (112, 'Srikakulam', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (113, 'Srikalahasti', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (115, 'Suryapet', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (116, 'Tadepalligudem', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (117, 'Tadpatri', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (118, 'Tandur', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (119, 'Tanuku', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (120, 'Tenali', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (121, 'Tirupati', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (122, 'Tuni', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (123, 'Uravakonda', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (124, 'Venkatagiri', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (125, 'Vicarabad', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (126, 'Vijayawada', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (127, 'Vinukonda', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (128, 'Visakhapatnam', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (129, 'Vizianagaram', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (130, 'Wanaparthy', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (131, 'Warangal', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (132, 'Yellandu', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (133, 'Yemmiganur', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (134, 'Yerraguntla', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (135, 'Zahirabad', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (136, 'Rajampet', 'Andhra Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (137, 'Along', 'Arunachal Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (138, 'Bomdila', 'Arunachal Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (139, 'Itanagar', 'Arunachal Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (140, 'Naharlagun', 'Arunachal Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (141, 'Pasighat', 'Arunachal Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (142, 'Abhayapuri', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (143, 'Amguri', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (144, 'Anandnagaar', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (145, 'Barpeta', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (146, 'Barpeta Road', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (147, 'Bilasipara', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (148, 'Bongaigaon', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (149, 'Dhekiajuli', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (150, 'Dhubri', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (151, 'Dibrugarh', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (152, 'Digboi', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (153, 'Diphu', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (154, 'Dispur', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (156, 'Gauripur', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (157, 'Goalpara', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (158, 'Golaghat', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (159, 'Guwahati', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (160, 'Haflong', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (161, 'Hailakandi', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (162, 'Hojai', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (163, 'Jorhat', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (164, 'Karimganj', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (165, 'Kokrajhar', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (166, 'Lanka', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (167, 'Lumding', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (168, 'Mangaldoi', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (169, 'Mankachar', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (170, 'Margherita', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (171, 'Mariani', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (172, 'Marigaon', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (173, 'Nagaon', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (174, 'Nalbari', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (175, 'North Lakhimpur', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (176, 'Rangia', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (177, 'Sibsagar', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (178, 'Silapathar', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (179, 'Silchar', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (180, 'Tezpur', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (181, 'Tinsukia', 'Assam');
        INSERT INTO cities(id, name, state_id) VALUES
        (182, 'Amarpur', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (183, 'Araria', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (184, 'Areraj', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (185, 'Arrah', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (186, 'Asarganj', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (187, 'Aurangabad', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (188, 'Bagaha', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (189, 'Bahadurganj', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (190, 'Bairgania', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (191, 'Bakhtiarpur', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (192, 'Banka', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (193, 'Banmankhi Bazar', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (194, 'Barahiya', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (195, 'Barauli', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (196, 'Barbigha', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (197, 'Barh', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (198, 'Begusarai', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (199, 'Behea', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (200, 'Bettiah', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (201, 'Bhabua', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (202, 'Bhagalpur', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (203, 'Bihar Sharif', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (204, 'Bikramganj', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (205, 'Bodh Gaya', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (206, 'Buxar', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (207, 'Chandan Bara', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (208, 'Chanpatia', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (209, 'Chhapra', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (210, 'Colgong', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (211, 'Dalsinghsarai', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (212, 'Darbhanga', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (213, 'Daudnagar', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (214, 'Dehri-on-Sone', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (215, 'Dhaka', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (216, 'Dighwara', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (217, 'Dumraon', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (218, 'Fatwah', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (219, 'Forbesganj', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (220, 'Gaya', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (221, 'Gogri Jamalpur', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (222, 'Gopalganj', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (223, 'Hajipur', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (224, 'Hilsa', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (225, 'Hisua', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (226, 'Islampur', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (227, 'Jagdispur', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (228, 'Jamalpur', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (229, 'Jamui', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (230, 'Jehanabad', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (231, 'Jhajha', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (232, 'Jhanjharpur', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (233, 'Jogabani', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (234, 'Kanti', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (235, 'Katihar', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (236, 'Khagaria', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (237, 'Kharagpur', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (238, 'Kishanganj', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (239, 'Lakhisarai', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (240, 'Lalganj', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (241, 'Madhepura', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (242, 'Madhubani', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (243, 'Maharajganj', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (244, 'Mahnar Bazar', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (245, 'Makhdumpur', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (246, 'Maner', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (247, 'Manihari', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (248, 'Marhaura', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (249, 'Masaurhi', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (250, 'Mirganj', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (251, 'Mokameh', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (252, 'Motihari', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (253, 'Motipur', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (254, 'Munger', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (255, 'Murliganj', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (256, 'Muzaffarpur', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (257, 'Narkatiaganj', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (258, 'Naugachhia', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (259, 'Nawada', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (260, 'Nokha', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (261, 'Patna', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (262, 'Piro', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (263, 'Purnia', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (264, 'Rafiganj', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (265, 'Rajgir', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (266, 'Ramnagar', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (267, 'Raxaul Bazar', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (268, 'Revelganj', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (269, 'Rosera', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (270, 'Saharsa', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (271, 'Samastipur', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (272, 'Sasaram', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (273, 'Sheikhpura', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (274, 'Sheohar', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (275, 'Sherghati', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (276, 'Silao', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (277, 'Sitamarhi', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (278, 'Siwan', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (279, 'Sonepur', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (280, 'Sugauli', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (281, 'Sultanganj', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (282, 'Supaul', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (283, 'Warisaliganj', 'Bihar');
        INSERT INTO cities(id, name, state_id) VALUES
        (284, 'Ahiwara', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (285, 'Akaltara', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (286, 'Ambagarh Chowki', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (287, 'Ambikapur', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (288, 'Arang', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (289, 'Bade Bacheli', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (290, 'Balod', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (291, 'Baloda Bazar', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (292, 'Bemetra', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (293, 'Bhatapara', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (294, 'Bilaspur', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (295, 'Birgaon', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (296, 'Champa', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (297, 'Chirmiri', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (298, 'Dalli-Rajhara', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (299, 'Dhamtari', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (300, 'Dipka', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (301, 'Dongargarh', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (302, 'Durg-Bhilai Nagar', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (303, 'Gobranawapara', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (304, 'Jagdalpur', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (305, 'Janjgir', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (306, 'Jashpurnagar', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (307, 'Kanker', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (308, 'Kawardha', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (309, 'Kondagaon', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (310, 'Korba', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (311, 'Mahasamund', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (312, 'Mahendragarh', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (313, 'Mungeli', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (314, 'Naila Janjgir', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (315, 'Raigarh', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (316, 'Raipur', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (317, 'Rajnandgaon', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (318, 'Sakti', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (319, 'Tilda Newra', 'Chhattisgarh');
        INSERT INTO cities(id, name, state_id) VALUES
        (320, 'Amli', 'Dadra & Nagar Haveli');
        INSERT INTO cities(id, name, state_id) VALUES
        (321, 'Silvassa', 'Dadra & Nagar Haveli');
        INSERT INTO cities(id, name, state_id) VALUES
        (322, 'Daman and Diu', 'Daman & Diu');
        INSERT INTO cities(id, name, state_id) VALUES
        (323, 'Daman and Diu', 'Daman & Diu');
        INSERT INTO cities(id, name, state_id) VALUES
        (324, 'Asola', 'Delhi');
        INSERT INTO cities(id, name, state_id) VALUES
        (325, 'Delhi', 'Delhi');
        INSERT INTO cities(id, name, state_id) VALUES
        (326, 'Aldona', 'Goa');
        INSERT INTO cities(id, name, state_id) VALUES
        (327, 'Curchorem Cacora', 'Goa');
        INSERT INTO cities(id, name, state_id) VALUES
        (328, 'Madgaon', 'Goa');
        INSERT INTO cities(id, name, state_id) VALUES
        (329, 'Mapusa', 'Goa');
        INSERT INTO cities(id, name, state_id) VALUES
        (330, 'Margao', 'Goa');
        INSERT INTO cities(id, name, state_id) VALUES
        (331, 'Marmagao', 'Goa');
        INSERT INTO cities(id, name, state_id) VALUES
        (332, 'Panaji', 'Goa');
        INSERT INTO cities(id, name, state_id) VALUES
        (333, 'Ahmedabad', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (334, 'Amreli', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (335, 'Anand', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (336, 'Ankleshwar', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (337, 'Bharuch', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (338, 'Bhavnagar', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (339, 'Bhuj', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (340, 'Cambay', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (341, 'Dahod', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (342, 'Deesa', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (343, 'Dharampur', ' India');
        INSERT INTO cities(id, name, state_id) VALUES
        (344, 'Dholka', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (345, 'Gandhinagar', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (346, 'Godhra', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (347, 'Himatnagar', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (348, 'Idar', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (349, 'Jamnagar', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (350, 'Junagadh', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (351, 'Kadi', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (352, 'Kalavad', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (353, 'Kalol', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (354, 'Kapadvanj', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (355, 'Karjan', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (356, 'Keshod', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (357, 'Khambhalia', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (358, 'Khambhat', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (359, 'Kheda', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (360, 'Khedbrahma', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (361, 'Kheralu', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (362, 'Kodinar', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (363, 'Lathi', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (364, 'Limbdi', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (365, 'Lunawada', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (366, 'Mahesana', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (367, 'Mahuva', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (368, 'Manavadar', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (369, 'Mandvi', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (370, 'Mangrol', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (371, 'Mansa', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (372, 'Mehmedabad', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (373, 'Modasa', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (374, 'Morvi', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (375, 'Nadiad', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (376, 'Navsari', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (377, 'Padra', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (378, 'Palanpur', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (379, 'Palitana', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (380, 'Pardi', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (381, 'Patan', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (382, 'Petlad', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (383, 'Porbandar', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (384, 'Radhanpur', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (385, 'Rajkot', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (386, 'Rajpipla', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (387, 'Rajula', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (388, 'Ranavav', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (389, 'Rapar', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (390, 'Salaya', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (391, 'Sanand', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (392, 'Savarkundla', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (393, 'Sidhpur', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (394, 'Sihor', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (395, 'Songadh', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (396, 'Surat', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (397, 'Talaja', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (398, 'Thangadh', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (399, 'Tharad', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (400, 'Umbergaon', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (401, 'Umreth', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (402, 'Una', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (403, 'Unjha', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (404, 'Upleta', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (405, 'Vadnagar', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (406, 'Vadodara', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (407, 'Valsad', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (408, 'Vapi', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (409, 'Vapi', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (410, 'Veraval', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (411, 'Vijapur', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (412, 'Viramgam', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (413, 'Visnagar', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (414, 'Vyara', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (415, 'Wadhwan', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (416, 'Wankaner', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (417, 'Adalaj', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (418, 'Adityana', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (419, 'Alang', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (420, 'Ambaji', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (421, 'Ambaliyasan', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (422, 'Andada', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (423, 'Anjar', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (424, 'Anklav', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (425, 'Antaliya', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (426, 'Arambhada', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (427, 'Atul', 'Gujarat');
        INSERT INTO cities(id, name, state_id) VALUES
        (428, 'Ballabhgarh', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (429, 'Ambala', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (430, 'Ambala', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (431, 'Asankhurd', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (432, 'Assandh', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (433, 'Ateli', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (434, 'Babiyal', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (435, 'Bahadurgarh', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (436, 'Barwala', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (437, 'Bhiwani', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (438, 'Charkhi Dadri', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (439, 'Cheeka', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (440, 'Ellenabad 2', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (441, 'Faridabad', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (442, 'Fatehabad', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (443, 'Ganaur', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (444, 'Gharaunda', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (445, 'Gohana', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (446, 'Gurgaon', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (447, 'Haibat(Yamuna Nagar)', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (448, 'Hansi', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (449, 'Hisar', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (450, 'Hodal', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (451, 'Jhajjar', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (452, 'Jind', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (453, 'Kaithal', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (454, 'Kalan Wali', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (455, 'Kalka', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (456, 'Karnal', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (457, 'Ladwa', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (458, 'Mahendragarh', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (459, 'Mandi Dabwali', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (460, 'Narnaul', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (461, 'Narwana', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (462, 'Palwal', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (463, 'Panchkula', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (464, 'Panipat', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (465, 'Pehowa', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (466, 'Pinjore', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (467, 'Rania', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (468, 'Ratia', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (469, 'Rewari', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (470, 'Rohtak', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (471, 'Safidon', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (472, 'Samalkha', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (473, 'Shahbad', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (474, 'Sirsa', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (475, 'Sohna', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (476, 'Sonipat', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (477, 'Taraori', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (478, 'Thanesar', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (479, 'Tohana', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (480, 'Yamunanagar', 'Haryana');
        INSERT INTO cities(id, name, state_id) VALUES
        (481, 'Arki', 'Himachal Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (482, 'Baddi', 'Himachal Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (483, 'Bilaspur', 'Himachal Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (484, 'Chamba', 'Himachal Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (485, 'Dalhousie', 'Himachal Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (486, 'Dharamsala', 'Himachal Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (487, 'Hamirpur', 'Himachal Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (488, 'Mandi', 'Himachal Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (489, 'Nahan', 'Himachal Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (490, 'Shimla', 'Himachal Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (491, 'Solan', 'Himachal Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (492, 'Sundarnagar', 'Himachal Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (493, 'Jammu', 'Jammu & Kashmir');
        INSERT INTO cities(id, name, state_id) VALUES
        (494, 'Achabbal', 'Jammu & Kashmir');
        INSERT INTO cities(id, name, state_id) VALUES
        (495, 'Akhnoor', 'Jammu & Kashmir');
        INSERT INTO cities(id, name, state_id) VALUES
        (496, 'Anantnag', 'Jammu & Kashmir');
        INSERT INTO cities(id, name, state_id) VALUES
        (497, 'Arnia', 'Jammu & Kashmir');
        INSERT INTO cities(id, name, state_id) VALUES
        (498, 'Awantipora', 'Jammu & Kashmir');
        INSERT INTO cities(id, name, state_id) VALUES
        (499, 'Bandipore', 'Jammu & Kashmir');
        INSERT INTO cities(id, name, state_id) VALUES
        (500, 'Baramula', 'Jammu & Kashmir');
        INSERT INTO cities(id, name, state_id) VALUES
        (501, 'Kathua', 'Jammu & Kashmir');
        INSERT INTO cities(id, name, state_id) VALUES
        (502, 'Leh', 'Jammu & Kashmir');
        INSERT INTO cities(id, name, state_id) VALUES
        (503, 'Punch', 'Jammu & Kashmir');
        INSERT INTO cities(id, name, state_id) VALUES
        (504, 'Rajauri', 'Jammu & Kashmir');
        INSERT INTO cities(id, name, state_id) VALUES
        (505, 'Sopore', 'Jammu & Kashmir');
        INSERT INTO cities(id, name, state_id) VALUES
        (506, 'Srinagar', 'Jammu & Kashmir');
        INSERT INTO cities(id, name, state_id) VALUES
        (507, 'Udhampur', 'Jammu & Kashmir');
        INSERT INTO cities(id, name, state_id) VALUES
        (508, 'Amlabad', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (509, 'Ara', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (510, 'Barughutu', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (511, 'Bokaro Steel City', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (512, 'Chaibasa', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (513, 'Chakradharpur', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (514, 'Chandrapura', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (515, 'Chatra', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (516, 'Chirkunda', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (517, 'Churi', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (518, 'Daltonganj', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (519, 'Deoghar', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (520, 'Dhanbad', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (521, 'Dumka', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (522, 'Garhwa', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (523, 'Ghatshila', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (524, 'Giridih', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (525, 'Godda', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (526, 'Gomoh', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (527, 'Gumia', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (528, 'Gumla', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (529, 'Hazaribag', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (530, 'Hussainabad', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (531, 'Jamshedpur', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (532, 'Jamtara', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (533, 'Jhumri Tilaiya', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (534, 'Khunti', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (535, 'Lohardaga', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (536, 'Madhupur', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (537, 'Mihijam', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (538, 'Musabani', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (539, 'Pakaur', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (540, 'Patratu', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (541, 'Phusro', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (542, 'Ramngarh', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (543, 'Ranchi', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (544, 'Sahibganj', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (545, 'Saunda', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (546, 'Simdega', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (547, 'Tenu Dam-cum- Kathhara', 'Jharkhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (548, 'Arasikere', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (549, 'Bangalore', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (550, 'Belgaum', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (551, 'Bellary', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (552, 'Chamrajnagar', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (553, 'Chikkaballapur', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (554, 'Chintamani', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (555, 'Chitradurga', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (556, 'Gulbarga', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (557, 'Gundlupet', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (558, 'Hassan', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (559, 'Hospet', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (560, 'Hubli', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (561, 'Karkala', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (562, 'Karwar', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (563, 'Kolar', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (564, 'Kota', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (565, 'Lakshmeshwar', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (566, 'Lingsugur', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (567, 'Maddur', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (568, 'Madhugiri', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (569, 'Madikeri', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (570, 'Magadi', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (571, 'Mahalingpur', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (572, 'Malavalli', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (573, 'Malur', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (574, 'Mandya', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (575, 'Mangalore', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (576, 'Manvi', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (577, 'Mudalgi', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (578, 'Mudbidri', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (579, 'Muddebihal', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (580, 'Mudhol', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (581, 'Mulbagal', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (582, 'Mundargi', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (583, 'Mysore', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (584, 'Nanjangud', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (585, 'Pavagada', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (586, 'Puttur', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (587, 'Rabkavi Banhatti', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (588, 'Raichur', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (589, 'Ramanagaram', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (590, 'Ramdurg', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (591, 'Ranibennur', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (592, 'Robertson Pet', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (593, 'Ron', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (594, 'Sadalgi', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (595, 'Sagar', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (596, 'Sakleshpur', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (597, 'Sandur', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (598, 'Sankeshwar', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (599, 'Saundatti-Yellamma', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (600, 'Savanur', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (601, 'Sedam', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (602, 'Shahabad', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (603, 'Shahpur', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (604, 'Shiggaon', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (605, 'Shikapur', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (606, 'Shimoga', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (607, 'Shorapur', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (608, 'Shrirangapattana', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (609, 'Sidlaghatta', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (610, 'Sindgi', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (611, 'Sindhnur', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (612, 'Sira', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (613, 'Sirsi', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (614, 'Siruguppa', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (615, 'Srinivaspur', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (616, 'Talikota', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (617, 'Tarikere', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (618, 'Tekkalakota', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (619, 'Terdal', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (620, 'Tiptur', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (621, 'Tumkur', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (622, 'Udupi', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (623, 'Vijayapura', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (624, 'Wadi', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (625, 'Yadgir', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (626, 'Adoor', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (627, 'Akathiyoor', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (628, 'Alappuzha', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (629, 'Ancharakandy', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (630, 'Aroor', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (631, 'Ashtamichira', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (632, 'Attingal', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (633, 'Avinissery', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (634, 'Chalakudy', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (635, 'Changanassery', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (636, 'Chendamangalam', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (637, 'Chengannur', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (638, 'Cherthala', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (639, 'Cheruthazham', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (640, 'Chittur-Thathamangalam', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (641, 'Chockli', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (642, 'Erattupetta', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (643, 'Guruvayoor', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (644, 'Irinjalakuda', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (645, 'Kadirur', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (646, 'Kalliasseri', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (647, 'Kalpetta', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (648, 'Kanhangad', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (649, 'Kanjikkuzhi', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (650, 'Kannur', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (651, 'Kasaragod', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (652, 'Kayamkulam', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (653, 'Kochi', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (654, 'Kodungallur', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (655, 'Kollam', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (656, 'Koothuparamba', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (657, 'Kothamangalam', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (658, 'Kottayam', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (659, 'Kozhikode', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (660, 'Kunnamkulam', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (661, 'Malappuram', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (662, 'Mattannur', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (663, 'Mavelikkara', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (664, 'Mavoor', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (665, 'Muvattupuzha', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (666, 'Nedumangad', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (667, 'Neyyattinkara', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (668, 'Ottappalam', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (669, 'Palai', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (670, 'Palakkad', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (671, 'Panniyannur', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (672, 'Pappinisseri', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (673, 'Paravoor', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (674, 'Pathanamthitta', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (675, 'Payyannur', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (676, 'Peringathur', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (677, 'Perinthalmanna', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (678, 'Perumbavoor', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (679, 'Ponnani', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (680, 'Punalur', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (681, 'Quilandy', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (682, 'Shoranur', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (683, 'Taliparamba', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (684, 'Thiruvalla', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (685, 'Thiruvananthapuram', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (686, 'Thodupuzha', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (687, 'Thrissur', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (688, 'Tirur', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (689, 'Vadakara', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (690, 'Vaikom', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (691, 'Varkala', 'Kerala');
        INSERT INTO cities(id, name, state_id) VALUES
        (692, 'Kavaratti', 'Lakshadweep');
        INSERT INTO cities(id, name, state_id) VALUES
        (693, 'Ashok Nagar', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (694, 'Balaghat', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (695, 'Betul', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (696, 'Bhopal', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (697, 'Burhanpur', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (698, 'Chhatarpur', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (699, 'Dabra', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (700, 'Datia', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (701, 'Dewas', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (702, 'Dhar', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (703, 'Fatehabad', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (704, 'Gwalior', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (705, 'Indore', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (706, 'Itarsi', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (707, 'Jabalpur', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (708, 'Katni', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (709, 'Kotma', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (710, 'Lahar', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (711, 'Lundi', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (712, 'Maharajpur', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (713, 'Mahidpur', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (714, 'Maihar', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (715, 'Malajkhand', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (716, 'Manasa', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (717, 'Manawar', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (718, 'Mandideep', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (719, 'Mandla', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (720, 'Mandsaur', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (721, 'Mauganj', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (722, 'Mhow Cantonment', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (723, 'Mhowgaon', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (724, 'Morena', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (725, 'Multai', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (726, 'Murwara', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (727, 'Nagda', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (728, 'Nainpur', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (729, 'Narsinghgarh', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (730, 'Narsinghgarh', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (731, 'Neemuch', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (732, 'Nepanagar', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (733, 'Niwari', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (734, 'Nowgong', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (735, 'Nowrozabad', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (736, 'Pachore', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (737, 'Pali', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (738, 'Panagar', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (739, 'Pandhurna', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (740, 'Panna', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (741, 'Pasan', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (742, 'Pipariya', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (743, 'Pithampur', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (744, 'Porsa', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (745, 'Prithvipur', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (746, 'Raghogarh-Vijaypur', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (747, 'Rahatgarh', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (748, 'Raisen', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (749, 'Rajgarh', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (750, 'Ratlam', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (751, 'Rau', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (752, 'Rehli', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (753, 'Rewa', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (754, 'Sabalgarh', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (755, 'Sagar', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (756, 'Sanawad', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (757, 'Sarangpur', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (758, 'Sarni', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (759, 'Satna', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (760, 'Sausar', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (761, 'Sehore', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (762, 'Sendhwa', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (763, 'Seoni', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (764, 'Seoni-Malwa', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (765, 'Shahdol', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (766, 'Shajapur', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (767, 'Shamgarh', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (768, 'Sheopur', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (769, 'Shivpuri', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (770, 'Shujalpur', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (771, 'Sidhi', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (772, 'Sihora', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (773, 'Singrauli', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (774, 'Sironj', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (775, 'Sohagpur', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (776, 'Tarana', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (777, 'Tikamgarh', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (778, 'Ujhani', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (779, 'Ujjain', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (780, 'Umaria', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (781, 'Vidisha', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (782, 'Wara Seoni', 'Madhya Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (783, 'Ahmednagar', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (784, 'Akola', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (785, 'Amravati', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (786, 'Aurangabad', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (787, 'Baramati', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (788, 'Chalisgaon', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (789, 'Chinchani', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (790, 'Devgarh', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (791, 'Dhule', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (792, 'Dombivli', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (793, 'Durgapur', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (794, 'Ichalkaranji', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (795, 'Jalna', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (796, 'Kalyan', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (797, 'Latur', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (798, 'Loha', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (799, 'Lonar', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (800, 'Lonavla', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (801, 'Mahad', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (802, 'Mahuli', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (803, 'Malegaon', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (804, 'Malkapur', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (805, 'Manchar', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (806, 'Mangalvedhe', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (807, 'Mangrulpir', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (808, 'Manjlegaon', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (809, 'Manmad', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (810, 'Manwath', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (811, 'Mehkar', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (812, 'Mhaswad', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (813, 'Miraj', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (814, 'Morshi', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (815, 'Mukhed', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (816, 'Mul', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (817, 'Mumbai', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (818, 'Murtijapur', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (819, 'Nagpur', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (820, 'Nalasopara', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (821, 'Nanded-Waghala', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (822, 'Nandgaon', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (823, 'Nandura', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (824, 'Nandurbar', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (825, 'Narkhed', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (826, 'Nashik', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (827, 'Navi Mumbai', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (828, 'Nawapur', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (829, 'Nilanga', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (830, 'Osmanabad', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (831, 'Ozar', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (832, 'Pachora', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (833, 'Paithan', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (834, 'Palghar', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (835, 'Pandharkaoda', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (836, 'Pandharpur', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (837, 'Panvel', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (838, 'Parbhani', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (839, 'Parli', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (840, 'Parola', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (841, 'Partur', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (842, 'Pathardi', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (843, 'Pathri', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (844, 'Patur', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (845, 'Pauni', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (846, 'Pen', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (847, 'Phaltan', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (848, 'Pulgaon', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (849, 'Pune', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (850, 'Purna', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (851, 'Pusad', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (852, 'Rahuri', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (853, 'Rajura', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (854, 'Ramtek', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (855, 'Ratnagiri', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (856, 'Raver', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (857, 'Risod', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (858, 'Sailu', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (859, 'Sangamner', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (860, 'Sangli', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (861, 'Sangole', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (862, 'Sasvad', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (863, 'Satana', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (864, 'Satara', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (865, 'Savner', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (866, 'Sawantwadi', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (867, 'Shahade', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (868, 'Shegaon', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (869, 'Shendurjana', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (870, 'Shirdi', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (871, 'Shirpur-Warwade', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (872, 'Shirur', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (873, 'Shrigonda', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (874, 'Shrirampur', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (875, 'Sillod', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (876, 'Sinnar', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (877, 'Solapur', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (878, 'Soyagaon', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (879, 'Talegaon Dabhade', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (880, 'Talode', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (881, 'Tasgaon', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (882, 'Tirora', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (883, 'Tuljapur', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (884, 'Tumsar', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (885, 'Uran', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (886, 'Uran Islampur', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (887, 'Wadgaon Road', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (888, 'Wai', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (889, 'Wani', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (890, 'Wardha', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (891, 'Warora', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (892, 'Warud', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (893, 'Washim', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (894, 'Yevla', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (895, 'Uchgaon', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (896, 'Udgir', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (897, 'Umarga', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (898, 'Umarkhed', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (899, 'Umred', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (900, 'Vadgaon Kasba', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (901, 'Vaijapur', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (902, 'Vasai', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (903, 'Virar', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (904, 'Vita', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (905, 'Yavatmal', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (906, 'Yawal', 'Maharashtra');
        INSERT INTO cities(id, name, state_id) VALUES
        (907, 'Imphal', 'Manipur');
        INSERT INTO cities(id, name, state_id) VALUES
        (908, 'Kakching', 'Manipur');
        INSERT INTO cities(id, name, state_id) VALUES
        (909, 'Lilong', 'Manipur');
        INSERT INTO cities(id, name, state_id) VALUES
        (910, 'Mayang Imphal', 'Manipur');
        INSERT INTO cities(id, name, state_id) VALUES
        (911, 'Thoubal', 'Manipur');
        INSERT INTO cities(id, name, state_id) VALUES
        (912, 'Jowai', 'Meghalaya');
        INSERT INTO cities(id, name, state_id) VALUES
        (913, 'Nongstoin', 'Meghalaya');
        INSERT INTO cities(id, name, state_id) VALUES
        (914, 'Shillong', 'Meghalaya');
        INSERT INTO cities(id, name, state_id) VALUES
        (915, 'Tura', 'Meghalaya');
        INSERT INTO cities(id, name, state_id) VALUES
        (916, 'Aizawl', 'Mizoram');
        INSERT INTO cities(id, name, state_id) VALUES
        (917, 'Champhai', 'Mizoram');
        INSERT INTO cities(id, name, state_id) VALUES
        (918, 'Lunglei', 'Mizoram');
        INSERT INTO cities(id, name, state_id) VALUES
        (919, 'Saiha', 'Mizoram');
        INSERT INTO cities(id, name, state_id) VALUES
        (920, 'Dimapur', 'Nagaland');
        INSERT INTO cities(id, name, state_id) VALUES
        (921, 'Kohima', 'Nagaland');
        INSERT INTO cities(id, name, state_id) VALUES
        (922, 'Mokokchung', 'Nagaland');
        INSERT INTO cities(id, name, state_id) VALUES
        (923, 'Tuensang', 'Nagaland');
        INSERT INTO cities(id, name, state_id) VALUES
        (924, 'Wokha', 'Nagaland');
        INSERT INTO cities(id, name, state_id) VALUES
        (925, 'Zunheboto', 'Nagaland');
        INSERT INTO cities(id, name, state_id) VALUES
        (950, 'Anandapur', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (951, 'Anugul', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (952, 'Asika', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (953, 'Balangir', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (954, 'Balasore', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (955, 'Baleshwar', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (956, 'Bamra', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (957, 'Barbil', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (958, 'Bargarh', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (959, 'Bargarh', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (960, 'Baripada', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (961, 'Basudebpur', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (962, 'Belpahar', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (963, 'Bhadrak', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (964, 'Bhawanipatna', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (965, 'Bhuban', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (966, 'Bhubaneswar', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (967, 'Biramitrapur', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (968, 'Brahmapur', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (969, 'Brajrajnagar', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (970, 'Byasanagar', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (971, 'Cuttack', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (972, 'Debagarh', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (973, 'Dhenkanal', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (974, 'Gunupur', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (975, 'Hinjilicut', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (976, 'Jagatsinghapur', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (977, 'Jajapur', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (978, 'Jaleswar', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (979, 'Jatani', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (980, 'Jeypur', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (981, 'Jharsuguda', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (982, 'Joda', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (983, 'Kantabanji', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (984, 'Karanjia', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (985, 'Kendrapara', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (986, 'Kendujhar', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (987, 'Khordha', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (988, 'Koraput', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (989, 'Malkangiri', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (990, 'Nabarangapur', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (991, 'Paradip', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (992, 'Parlakhemundi', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (993, 'Pattamundai', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (994, 'Phulabani', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (995, 'Puri', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (996, 'Rairangpur', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (997, 'Rajagangapur', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (998, 'Raurkela', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (999, 'Rayagada', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (1000, 'Sambalpur', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (1001, 'Soro', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (1002, 'Sunabeda', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (1003, 'Sundargarh', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (1004, 'Talcher', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (1005, 'Titlagarh', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (1006, 'Umarkote', 'Odisha');
        INSERT INTO cities(id, name, state_id) VALUES
        (1007, 'Karaikal', 'Puducherry');
        INSERT INTO cities(id, name, state_id) VALUES
        (1008, 'Mahe', 'Puducherry');
        INSERT INTO cities(id, name, state_id) VALUES
        (1009, 'Puducherry', 'Puducherry');
        INSERT INTO cities(id, name, state_id) VALUES
        (1010, 'Yanam', 'Puducherry');
        INSERT INTO cities(id, name, state_id) VALUES
        (1011, 'Ahmedgarh', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1012, 'Amritsar', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1013, 'Barnala', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1014, 'Batala', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1015, 'Bathinda', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1016, 'Bhagha Purana', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1017, 'Budhlada', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1018, 'Chandigarh', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1019, 'Dasua', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1020, 'Dhuri', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1021, 'Dinanagar', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1022, 'Faridkot', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1023, 'Fazilka', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1024, 'Firozpur', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1025, 'Firozpur Cantt.', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1026, 'Giddarbaha', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1027, 'Gobindgarh', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1028, 'Gurdaspur', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1029, 'Hoshiarpur', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1030, 'Jagraon', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1031, 'Jaitu', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1032, 'Jalalabad', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1033, 'Jalandhar', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1034, 'Jalandhar Cantt.', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1035, 'Jandiala', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1036, 'Kapurthala', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1037, 'Karoran', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1038, 'Kartarpur', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1039, 'Khanna', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1040, 'Kharar', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1041, 'Kot Kapura', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1042, 'Kurali', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1043, 'Longowal', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1044, 'Ludhiana', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1045, 'Malerkotla', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1046, 'Malout', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1047, 'Mansa', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1048, 'Maur', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1049, 'Moga', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1050, 'Mohali', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1051, 'Morinda', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1052, 'Mukerian', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1053, 'Muktsar', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1054, 'Nabha', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1055, 'Nakodar', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1056, 'Nangal', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1057, 'Nawanshahr', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1058, 'Pathankot', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1059, 'Patiala', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1060, 'Patran', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1061, 'Patti', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1062, 'Phagwara', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1063, 'Phillaur', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1064, 'Qadian', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1065, 'Raikot', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1066, 'Rajpura', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1067, 'Rampura Phul', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1068, 'Rupnagar', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1069, 'Samana', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1070, 'Sangrur', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1071, 'Sirhind Fatehgarh Sahib', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1072, 'Sujanpur', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1073, 'Sunam', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1074, 'Talwara', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1075, 'Tarn Taran', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1076, 'Urmar Tanda', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1077, 'Zira', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1078, 'Zirakpur', 'Punjab');
        INSERT INTO cities(id, name, state_id) VALUES
        (1079, 'Bali', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1080, 'Banswara', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1081, 'Ajmer', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1082, 'Alwar', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1083, 'Bandikui', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1084, 'Baran', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1085, 'Barmer', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1086, 'Bikaner', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1087, 'Fatehpur', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1088, 'Jaipur', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1089, 'Jaisalmer', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1090, 'Jodhpur', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1091, 'Kota', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1092, 'Lachhmangarh', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1093, 'Ladnu', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1094, 'Lakheri', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1095, 'Lalsot', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1096, 'Losal', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1097, 'Makrana', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1098, 'Malpura', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1099, 'Mandalgarh', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1100, 'Mandawa', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1101, 'Mangrol', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1102, 'Merta City', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1103, 'Mount Abu', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1104, 'Nadbai', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1105, 'Nagar', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1106, 'Nagaur', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1107, 'Nargund', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1108, 'Nasirabad', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1109, 'Nathdwara', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1110, 'Navalgund', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1111, 'Nawalgarh', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1112, 'Neem-Ka-Thana', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1113, 'Nelamangala', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1114, 'Nimbahera', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1115, 'Nipani', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1116, 'Niwai', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1117, 'Nohar', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1118, 'Nokha', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1119, 'Pali', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1120, 'Phalodi', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1121, 'Phulera', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1122, 'Pilani', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1123, 'Pilibanga', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1124, 'Pindwara', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1125, 'Pipar City', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1126, 'Prantij', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1127, 'Pratapgarh', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1128, 'Raisinghnagar', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1129, 'Rajakhera', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1130, 'Rajaldesar', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1131, 'Rajgarh (Alwar)', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1132, 'Rajgarh (Churu', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1133, 'Rajsamand', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1134, 'Ramganj Mandi', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1135, 'Ramngarh', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1136, 'Ratangarh', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1137, 'Rawatbhata', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1138, 'Rawatsar', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1139, 'Reengus', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1140, 'Sadri', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1141, 'Sadulshahar', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1142, 'Sagwara', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1143, 'Sambhar', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1144, 'Sanchore', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1145, 'Sangaria', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1146, 'Sardarshahar', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1147, 'Sawai Madhopur', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1148, 'Shahpura', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1149, 'Shahpura', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1150, 'Sheoganj', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1151, 'Sikar', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1152, 'Sirohi', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1153, 'Sojat', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1154, 'Sri Madhopur', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1155, 'Sujangarh', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1156, 'Sumerpur', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1157, 'Suratgarh', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1158, 'Taranagar', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1159, 'Todabhim', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1160, 'Todaraisingh', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1161, 'Tonk', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1162, 'Udaipur', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1163, 'Udaipurwati', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1164, 'Vijainagar', 'Rajasthan');
        INSERT INTO cities(id, name, state_id) VALUES
        (1165, 'Gangtok', 'Sikkim');
        INSERT INTO cities(id, name, state_id) VALUES
        (1166, 'Calcutta', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1167, 'Arakkonam', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1168, 'Arcot', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1169, 'Aruppukkottai', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1170, 'Bhavani', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1171, 'Chengalpattu', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1172, 'Chennai', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1173, 'Chinna salem', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1174, 'Coimbatore', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1175, 'Coonoor', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1176, 'Cuddalore', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1177, 'Dharmapuri', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1178, 'Dindigul', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1179, 'Erode', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1180, 'Gudalur', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1181, 'Gudalur', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1182, 'Gudalur', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1183, 'Kanchipuram', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1184, 'Karaikudi', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1185, 'Karungal', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1186, 'Karur', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1187, 'Kollankodu', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1188, 'Lalgudi', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1189, 'Madurai', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1190, 'Nagapattinam', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1191, 'Nagercoil', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1192, 'Namagiripettai', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1193, 'Namakkal', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1194, 'Nandivaram-Guduvancheri', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1195, 'Nanjikottai', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1196, 'Natham', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1197, 'Nellikuppam', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1198, 'Neyveli', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1199, 'O'' Valley', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1200, 'Oddanchatram', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1201, 'P.N.Patti', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1202, 'Pacode', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1203, 'Padmanabhapuram', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1204, 'Palani', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1205, 'Palladam', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1206, 'Pallapatti', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1207, 'Pallikonda', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1208, 'Panagudi', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1209, 'Panruti', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1210, 'Paramakudi', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1211, 'Parangipettai', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1212, 'Pattukkottai', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1213, 'Perambalur', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1214, 'Peravurani', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1215, 'Periyakulam', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1216, 'Periyasemur', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1217, 'Pernampattu', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1218, 'Pollachi', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1219, 'Polur', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1220, 'Ponneri', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1221, 'Pudukkottai', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1222, 'Pudupattinam', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1223, 'Puliyankudi', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1224, 'Punjaipugalur', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1225, 'Rajapalayam', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1226, 'Ramanathapuram', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1227, 'Rameshwaram', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1228, 'Rasipuram', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1229, 'Salem', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1230, 'Sankarankoil', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1231, 'Sankari', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1232, 'Sathyamangalam', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1233, 'Sattur', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1234, 'Shenkottai', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1235, 'Sholavandan', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1236, 'Sholingur', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1237, 'Sirkali', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1238, 'Sivaganga', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1239, 'Sivagiri', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1240, 'Sivakasi', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1241, 'Srivilliputhur', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1242, 'Surandai', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1243, 'Suriyampalayam', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1244, 'Tenkasi', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1245, 'Thammampatti', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1246, 'Thanjavur', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1247, 'Tharamangalam', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1248, 'Tharangambadi', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1249, 'Theni Allinagaram', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1250, 'Thirumangalam', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1251, 'Thirunindravur', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1252, 'Thiruparappu', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1253, 'Thirupuvanam', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1254, 'Thiruthuraipoondi', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1255, 'Thiruvallur', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1256, 'Thiruvarur', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1257, 'Thoothukudi', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1258, 'Thuraiyur', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1259, 'Tindivanam', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1260, 'Tiruchendur', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1261, 'Tiruchengode', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1262, 'Tiruchirappalli', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1263, 'Tirukalukundram', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1264, 'Tirukkoyilur', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1265, 'Tirunelveli', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1266, 'Tirupathur', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1267, 'Tirupathur', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1268, 'Tiruppur', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1269, 'Tiruttani', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1270, 'Tiruvannamalai', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1271, 'Tiruvethipuram', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1272, 'Tittakudi', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1273, 'Udhagamandalam', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1274, 'Udumalaipettai', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1275, 'Unnamalaikadai', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1276, 'Usilampatti', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1277, 'Uthamapalayam', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1278, 'Uthiramerur', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1279, 'Vadakkuvalliyur', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1280, 'Vadalur', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1281, 'Vadipatti', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1282, 'Valparai', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1283, 'Vandavasi', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1284, 'Vaniyambadi', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1285, 'Vedaranyam', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1286, 'Vellakoil', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1287, 'Vellore', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1288, 'Vikramasingapuram', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1289, 'Viluppuram', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1290, 'Virudhachalam', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1291, 'Virudhunagar', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1292, 'Viswanatham', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1293, 'Agartala', 'Tripura');
        INSERT INTO cities(id, name, state_id) VALUES
        (1294, 'Badharghat', 'Tripura');
        INSERT INTO cities(id, name, state_id) VALUES
        (1295, 'Dharmanagar', 'Tripura');
        INSERT INTO cities(id, name, state_id) VALUES
        (1296, 'Indranagar', 'Tripura');
        INSERT INTO cities(id, name, state_id) VALUES
        (1297, 'Jogendranagar', 'Tripura');
        INSERT INTO cities(id, name, state_id) VALUES
        (1298, 'Kailasahar', 'Tripura');
        INSERT INTO cities(id, name, state_id) VALUES
        (1299, 'Khowai', 'Tripura');
        INSERT INTO cities(id, name, state_id) VALUES
        (1300, 'Pratapgarh', 'Tripura');
        INSERT INTO cities(id, name, state_id) VALUES
        (1301, 'Udaipur', 'Tripura');
        INSERT INTO cities(id, name, state_id) VALUES
        (1302, 'Achhnera', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1303, 'Adari', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1304, 'Agra', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1305, 'Aligarh', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1306, 'Allahabad', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1307, 'Amroha', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1308, 'Azamgarh', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1309, 'Bahraich', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1310, 'Ballia', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1311, 'Balrampur', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1312, 'Banda', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1313, 'Bareilly', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1314, 'Chandausi', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1315, 'Dadri', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1316, 'Deoria', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1317, 'Etawah', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1318, 'Fatehabad', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1319, 'Fatehpur', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1320, 'Fatehpur', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1321, 'Greater Noida', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1322, 'Hamirpur', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1323, 'Hardoi', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1324, 'Jajmau', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1325, 'Jaunpur', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1326, 'Jhansi', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1327, 'Kalpi', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1328, 'Kanpur', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1329, 'Kota', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1330, 'Laharpur', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1331, 'Lakhimpur', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1332, 'Lal Gopalganj Nindaura', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1333, 'Lalganj', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1334, 'Lalitpur', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1335, 'Lar', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1336, 'Loni', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1337, 'Lucknow', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1338, 'Mathura', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1339, 'Meerut', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1340, 'Modinagar', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1341, 'Muradnagar', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1342, 'Nagina', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1343, 'Najibabad', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1344, 'Nakur', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1345, 'Nanpara', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1346, 'Naraura', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1347, 'Naugawan Sadat', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1348, 'Nautanwa', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1349, 'Nawabganj', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1350, 'Nehtaur', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1351, 'NOIDA', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1352, 'Noorpur', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1353, 'Obra', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1354, 'Orai', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1355, 'Padrauna', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1356, 'Palia Kalan', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1357, 'Parasi', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1358, 'Phulpur', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1359, 'Pihani', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1360, 'Pilibhit', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1361, 'Pilkhuwa', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1362, 'Powayan', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1363, 'Pukhrayan', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1364, 'Puranpur', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1365, 'Purquazi', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1366, 'Purwa', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1367, 'Rae Bareli', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1368, 'Rampur', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1369, 'Rampur Maniharan', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1370, 'Rasra', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1371, 'Rath', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1372, 'Renukoot', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1373, 'Reoti', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1374, 'Robertsganj', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1375, 'Rudauli', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1376, 'Rudrapur', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1377, 'Sadabad', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1378, 'Safipur', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1379, 'Saharanpur', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1380, 'Sahaspur', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1381, 'Sahaswan', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1382, 'Sahawar', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1383, 'Sahjanwa', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1384, 'Saidpur', ' Ghazipur');
        INSERT INTO cities(id, name, state_id) VALUES
        (1385, 'Sambhal', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1386, 'Samdhan', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1387, 'Samthar', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1388, 'Sandi', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1389, 'Sandila', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1390, 'Sardhana', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1391, 'Seohara', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1392, 'Shahabad', ' Hardoi');
        INSERT INTO cities(id, name, state_id) VALUES
        (1393, 'Shahabad', ' Rampur');
        INSERT INTO cities(id, name, state_id) VALUES
        (1394, 'Shahganj', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1395, 'Shahjahanpur', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1396, 'Shamli', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1397, 'Shamsabad', ' Agra');
        INSERT INTO cities(id, name, state_id) VALUES
        (1398, 'Shamsabad', ' Farrukhabad');
        INSERT INTO cities(id, name, state_id) VALUES
        (1399, 'Sherkot', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1400, 'Shikarpur', ' Bulandshahr');
        INSERT INTO cities(id, name, state_id) VALUES
        (1401, 'Shikohabad', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1402, 'Shishgarh', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1403, 'Siana', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1404, 'Sikanderpur', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1405, 'Sikandra Rao', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1406, 'Sikandrabad', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1407, 'Sirsaganj', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1408, 'Sirsi', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1409, 'Sitapur', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1410, 'Soron', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1411, 'Suar', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1412, 'Sultanpur', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1413, 'Sumerpur', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1414, 'Tanda', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1415, 'Tanda', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1416, 'Tetri Bazar', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1417, 'Thakurdwara', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1418, 'Thana Bhawan', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1419, 'Tilhar', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1420, 'Tirwaganj', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1421, 'Tulsipur', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1422, 'Tundla', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1423, 'Unnao', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1424, 'Utraula', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1425, 'Varanasi', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1426, 'Vrindavan', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1427, 'Warhapur', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1428, 'Zaidpur', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1429, 'Zamania', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1430, 'Almora', 'Uttarakhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (1431, 'Bazpur', 'Uttarakhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (1432, 'Chamba', 'Uttarakhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (1433, 'Dehradun', 'Uttarakhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (1434, 'Haldwani', 'Uttarakhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (1435, 'Haridwar', 'Uttarakhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (1436, 'Jaspur', 'Uttarakhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (1437, 'Kashipur', 'Uttarakhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (1438, 'kichha', 'Uttarakhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (1439, 'Kotdwara', 'Uttarakhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (1440, 'Manglaur', 'Uttarakhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (1441, 'Mussoorie', 'Uttarakhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (1442, 'Nagla', 'Uttarakhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (1443, 'Nainital', 'Uttarakhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (1444, 'Pauri', 'Uttarakhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (1445, 'Pithoragarh', 'Uttarakhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (1446, 'Ramnagar', 'Uttarakhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (1447, 'Rishikesh', 'Uttarakhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (1448, 'Roorkee', 'Uttarakhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (1449, 'Rudrapur', 'Uttarakhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (1450, 'Sitarganj', 'Uttarakhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (1451, 'Tehri', 'Uttarakhand');
        INSERT INTO cities(id, name, state_id) VALUES
        (1452, 'Muzaffarnagar', 'Uttar Pradesh');
        INSERT INTO cities(id, name, state_id) VALUES
        (1453, 'Adra', ' Purulia');
        INSERT INTO cities(id, name, state_id) VALUES
        (1454, 'Alipurduar', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1455, 'Arambagh', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1456, 'Asansol', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1457, 'Baharampur', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1458, 'Bally', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1459, 'Balurghat', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1460, 'Bankura', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1461, 'Barakar', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1462, 'Barasat', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1463, 'Bardhaman', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1464, 'Bidhan Nagar', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1465, 'Chinsura', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1466, 'Contai', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1467, 'Cooch Behar', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1468, 'Darjeeling', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1469, 'Durgapur', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1470, 'Haldia', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1471, 'Howrah', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1472, 'Islampur', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1473, 'Jhargram', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1474, 'Kharagpur', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1475, 'Kolkata', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1476, 'Mainaguri', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1477, 'Mal', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1478, 'Mathabhanga', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1479, 'Medinipur', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1480, 'Memari', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1481, 'Monoharpur', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1482, 'Murshidabad', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1483, 'Nabadwip', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1484, 'Naihati', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1485, 'Panchla', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1486, 'Pandua', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1487, 'Paschim Punropara', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1488, 'Purulia', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1489, 'Raghunathpur', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1490, 'Raiganj', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1491, 'Rampurhat', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1492, 'Ranaghat', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1493, 'Sainthia', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1494, 'Santipur', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1495, 'Siliguri', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1496, 'Sonamukhi', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1497, 'Srirampore', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1498, 'Suri', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1499, 'Taki', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1500, 'Tamluk', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1501, 'Tarakeswar', 'West Bengal');
        INSERT INTO cities(id, name, state_id) VALUES
        (1502, 'Chikmagalur', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (1503, 'Davanagere', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (1504, 'Dharwad', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (1505, 'Gadag', 'Karnataka');
        INSERT INTO cities(id, name, state_id) VALUES
        (1506, 'Chennai', 'Tamil Nadu');
        INSERT INTO cities(id, name, state_id) VALUES
        (1507, 'Coimbatore', 'Tamil Nadu');
        }
    }
