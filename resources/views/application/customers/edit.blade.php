@extends('layouts.app', ['page' => 'customers'])

@section('title', __('messages.create_customer'))
    
@section('page_header')
    <div class="page__heading d-flex align-items-center">
        <div class="flex">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0">
                    <li class="breadcrumb-item"><a href="#"><i class="material-icons icon-20pt">home</i></a></li>
                    <li class="breadcrumb-item" aria-current="page"><a href="{{ route('customers', ['company_uid' => $currentCompany->uid]) }}">{{ __('messages.customers') }}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{ __('messages.update_customer') }}</li>
                </ol>
            </nav>
            <h1 class="m-0 h3">{{ __('messages.update_customer') }}</h1>
        </div>
    </div>
@endsection
 
@section('content') 
    <form action="{{ route('customers.update', ['customer' => $customer->id, 'company_uid' => $currentCompany->uid]) }}" method="POST" enctype="multipart/form-data">
        @include('layouts._form_errors')
        @csrf
        
        @include('application.customers._form')
    </form>
@endsection

@section('page_body_scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('.oi').on('change', function() {
            
            var stateID = $(this).val();
            console.log(stateID);
            console.log("s")
            if(stateID) {
            console.log("s")

                $.ajax({
                    url: "{{ route('application.customer.city') }}",
                    type: "GET",
                    data: { id:stateID },
                    success:function(data) {
                    //    console.log(data);
                        
                        $('select[name="billing[city]"]').empty();
                        $.each(data, function(key, value) {
                            // console.log(value.name);
                            $('select[name="billing[city]"]').append('<option value="'+ value.name +'">'+ value.name +'</option>');
                        });

                        $('select[name="shipping[city]"]').empty();
                        $.each(data, function(key, value) {
                            // console.log(value.name);
                            $('select[name="shipping[city]"]').append('<option value="'+ value.name +'">'+ value.name +'</option>');
                        });


                    }
                });
            }else{
                $('select[name="city"]').empty();
            }
        });
    });
</script>

<script>
    
    
    $(document).ready(function(){
        $('#checkbox').click(function(){
        if($('input[name="checkout"]').is(':checked')){
        $('#shipping_name').val($('#billing_name').val());
            // console.log($('shipping_name').val($('#billing_name').val())); 
        $('#shipping_phone').val($('#billing_phone').val());
        $('#shipping_country_id').val($('#billing_country_id').val());
        $('#shipping_state').val($('#billing_state').val());
        $('#shipping_city').val($('#billing_city').val());
        $('#shipping_zip').val($('#billing_zip').val());
        $('#shipping_address').val($('#billing_address').val());
        var country = $('#billing_country_id option:selected').val();
        if(country !="")
        {
        $('#shipping_country_id option[value="' + country + '"]').prop('selected', true);
        }
        } else { 
        $('#shipping_address').val("");
        $('#shipping_name').val("");
        $('#shipping_state').val("");
        $('#shipping_city').val("");
        $('#shipping_zip').val("");
        $('#shipping_phone').val("");
        $('#shipping_country_id option:eq(0)').prop('selected', true);
        };
        
        });
        });
    
    
    </script> 

@endsection