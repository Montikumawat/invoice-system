<table width="100%" class="items-table" cellspacing="0" border="0">
    <tr class="item-table-heading-row">
        <th width="2%" class="pr-20 text-right item-table-heading">#</th>
        @if($estimate->tax_per_item)
            <th width="30%" class="pl-0 text-left item-table-heading">{{ __('messages.product') }}</th>
        @else
            <th width="40%" class="pl-0 text-left item-table-heading">{{ __('messages.product') }}</th>
        @endif
        <th class="pr-20 text-right item-table-heading">{{ __('messages.quantity') }}</th>
        <th class="pr-20 text-right item-table-heading">{{ __('messages.price') }}</th>
        {{-- @if($estimate->tax_per_item) --}}
        
        @if( $estimate->state_id == auth()->user()->state_id )

            <th width="20%" class="pl-10 text-right item-table-heading">SGST</th>

            <th width="20%" class="pl-10 text-right item-table-heading">CGST</th>



        @else

            <th width="20%" class="pl-10 text-right item-table-heading">IGST</th>
        @endif
        {{-- @endif --}}
        @if($estimate->discount_per_item)
            <th class="pl-10 text-right item-table-heading">{{ __('messages.discount') }}</th>
        @endif
        <th class="text-right item-table-heading">{{ __('messages.amount') }}</th>
    </tr>
    @php
        $index = 1
    @endphp
    @foreach ($estimate->items as $item)
        <tr class="item-row">
            <td class="pr-20 text-right item-cell" style="vertical-align: top;">
                {{$index}}
            </td>
            <td class="pl-0 text-left item-cell">
                <span>{{ $item->product->name }}</span><br>
                <span class="item-description">
                    {!! nl2br(htmlspecialchars($item->product->description)) !!}
                </span>
            </td>
            <td class="pr-20 text-right item-cell" style="vertical-align: top;">
                {{ $item->quantity }}
            </td>
            <td class="pr-20 text-right item-cell" style="vertical-align: top;">
                {!! money(($item->price) * 100, $estimate->currency_code)->format() !!}
            </td>
            {{-- @if($estimate->tax_per_item)
                <td class="pl-10 text-right item-cell" style="vertical-align: top;">
                    @foreach ($item->getTotalPercentageOfTaxesWithNames() as $key => $value)
                        {{$key . ' ('. $value. '%' .')'}}
                    @endforeach
                </td>
            @endif --}}
            @foreach ($item->getTotalPercentageOfTaxesWithNames() as $key => $value)
        
            @if( $estimate->state_id == auth()->user()->state_id )
            
            
                <td class="pr-20 text-right item-cell" style="vertical-align: top;">
    
                
    
                    {{(money((($estimate->sub_total)* ($value/200))*100, $estimate->currency_code)->format()) }} ({{ ($value/2)}}%)
                        
                </td>
        
        
            
                <td class="pr-20 text-right item-cell" style="vertical-align: top;">
                    {{(money((($estimate->sub_total)* ($value/200))*100, $estimate->currency_code)->format())}} ({{ ($value/2)}}%)
    
                
                        
                </td>
    
            @else
        
            
                <td class="pr-20 text-right item-cell" style="vertical-align: top;">
                    {{-- {{(money(((($estimate->sub_total)*100)* ($value/100))*100, $estimate->currency_code)->format())}} --}}
    
                    {{(money((($estimate->sub_total)* ($value/100))*100, $estimate->currency_code)->format())}} ({{ ($value)}}%)
                        
                </td>
       
            @endif

       


        @endforeach
            @if($estimate->discount_per_item)
                <td class="pl-10 text-right item-cell" style="vertical-align: top;">
                    {{ $item->discount_val }}% 
                </td>
            @endif
            <td class="text-right item-cell" style="vertical-align: top;">
                {!! money(($item->total) * 100, $estimate->currency_code)->format() !!}
            </td>
        </tr>
        @php
            $index += 1
        @endphp
    @endforeach
</table>

<hr class="item-cell-table-hr">



<div class="total-display-container">
    <table width="100%" cellspacing="0px" border="0" class="SubTotalFormat @if(count($estimate->items) > 12) page-break @endif">
        <tr>
            <td class="border-0 total-table-attribute-label"><h4>{{ __('messages.sub_total') }}</h4></td>
            <td class="py-2 border-0 item-cell total-table-attribute-value">
                @if($estimate->tax_per_item  == false)

                {!! money(($estimate->sub_total)*100, $estimate->currency_code)->format() !!}
                {{-- {{dd( money(($estimate->getItemsSubTotalByBasePrice())*100, $estimate->currency_code)->format())}} --}}
                
                @else
                    {!! money(($estimate->getItemsSubTotalByBasePrice())*100, $estimate->currency_code)->format() !!}
                    
                @endif
           
            </td>
        </tr>
      
        @if ($estimate->discount_per_item == false)
        @if($estimate->discount_val > 0)
        <tr>
            <td class="border-0 total-table-attribute-label">
                <h4>{{ __('messages.discount') . ' (' . $estimate->discount_val . '%)' }}</h4>
            </td>
            <td class="py-2 border-0 item-cell total-table-attribute-value">
                - {!! money((($estimate->discount_val / 100) * $estimate->sub_total)*100, $estimate->currency_code)->format() !!}
            </td>
        </tr>
        @endif
        @else 
        @php $discount_val = $estimate->getItemsTotalDiscount() @endphp
        @if($discount_val > 0)
        <tr>
            <td class="border-0 total-table-attribute-label">
                <h4>{{ __('messages.discount') }}</h4>
            </td>
            <td class="py-2 border-0 item-cell total-table-attribute-value">
                - {!! money($discount_val, $estimate->currency_code)->format() !!}
            </td>
            
        </tr>
        @endif
        
        @if($estimate->tax_per_item  == false)
            @foreach ($estimate->getTotalPercentageOfTaxesWithNames() as $key => $value)
                <tr>
                    <td class="border-0 total-table-attribute-label">
                        <h4>Tax</h4>
                    </td>
                    <td class="border-0 item-cell total-table-attribute-value">
                        {!! money((($value / 100) * $estimate->sub_total)*100, $estimate->currency_code)->format() !!}
                        
                    </td>
                </tr>
            @endforeach
        @else
            @foreach ($estimate->getItemsTotalPercentageOfTaxesWithNames() as $key => $value)
                <tr>
                    <td class="border-0 total-table-attribute-label">
                        <h4>{{$key}}</h4>
                    </td>
                    <td class="border-0 item-cell total-table-attribute-value">
                        {!! money($value, $estimate->currency_code)->format() !!}
                    </td>
                </tr>
            @endforeach
        @endif

       
        @endif
            @php
                $tax = 0;
            @endphp
            @foreach ($estimate->items as $item)
                @foreach ($item->getTotalPercentageOfTaxesWithNames() as $key => $value)
                    @php
                        $tax += (($estimate->sub_total)* ($value/100))*100;
                    @endphp
                @endforeach
            @endforeach
            
        {{-- @if( $estimate->state_id == auth()->user()->state_id )
        <tr>
            <td class="border-0 total-table-attribute-label">
                <h4>SGST @ {{ ($value/2)}}%</h4>
            </td>
            <td class="py-2 border-0 item-cell total-table-attribute-value">

               

                {{(money((($estimate->sub_total)* ($value/200))*100, $estimate->currency_code)->format())}}
                     
            </td>
        </tr>
        <tr>
            <td class="total-table-attribute-label">
                <h4>CGST @ {{ ($value/2)}}%</h4>
            </td>
            <td class="py-2 border-0 item-cell total-table-attribute-value">
                {{(money((($estimate->sub_total)* ($value/200))*100, $estimate->currency_code)->format())}}

            
                     
            </td>
        </tr>
        @else --}}
        {{-- {{dd($tax)}} --}}
        <tr>
            <td class=" total-table-attribute-label">
                <h4>Tax</h4>
            </td>
            <td class="py-2 border-0 item-cell total-table-attribute-value">
                {{-- {{(money(((($estimate->sub_total)*100)* ($value/100))*100, $estimate->currency_code)->format())}} --}}

                {{(money($tax, $estimate->currency_code)->format())}}
                     
            </td>
        </tr>
        {{-- @endif --}}

       


        <tr>
            <td class="py-3"></td>
        </tr>
        <tr>
            <td class="border-0 total-border-left total-table-attribute-label">
                <h3>{{ __('messages.total') }}</h3>
            </td>
            <td class="py-8 border-0 total-border-right item-cell total-table-attribute-value">
                {{-- {{dd($estimate->items[0]->total)}} --}}
                @php
                    $total = 0;
                @endphp
                @foreach ($estimate->items as $item)
                    @php
                        $total = $item->total;
                    @endphp
                @endforeach
                {{-- {{$total}} --}}
                <h3>{!! money(($estimate->total)*100, $estimate->currency_code)->format()  !!}</h3>
            </td>
        </tr>
    </table>
</div> 
